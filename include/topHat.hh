#ifndef TOPHAT_HH_
# define TOPHAT_HH_

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdint>
#include <algorithm>
#include <chrono>

using namespace cv;
using namespace std::chrono;

Mat reduce_num_colors(Mat &src);
Mat dilate_image(const Mat &src);
Mat erode_image(const Mat &src);
Mat top_hat_black(const Mat &src);
Mat closing(const Mat &src);
Mat top_hat_black_opencv(const Mat &src, Mat &dst);

#endif /* !TOPHAT_HH_ */
