#ifndef SHAPERECOGNITION_HH_
# define SHAPERECOGNITION_HH_

#include <opencv2/opencv.hpp>
# include "../include/Cluster.hh"

using namespace cv;

class ShapeRecognition
{
    public:
        ShapeRecognition();
        virtual ~ShapeRecognition();
        void drawContours(Mat &src, int i, Vec3b color);
        Mat big_contours_recognition(const Mat &src);
        std::vector<std::vector<int> > merge_clusters(
                std::vector<std::vector<int> > clusters);
        std::vector<std::vector<int> > remove_little_clusters(
                std::vector<std::vector<int> > clusters);
        Mat clusters_colored(const Mat &src,
                std::vector<std::vector<int> > clusters);
        Mat make_clusters(const Mat &src);
        Mat remove_useless_contours(const Mat &src);
        int euclidean_distance(Point A, Point B);
        std::vector<std::vector<int> > getClusters();

        std::vector<Cluster> Clusters;
        std::vector<std::vector<int> > clusters;
        std::vector<std::pair<std::vector<Point>, int> > ctrs_ids;

    private:
        std::vector<bool> horizontals;
        std::vector<bool> vertical;
        std::vector<bool> diagonal;
};

#endif /* !SHAPERECOGNITION_HH_ */
