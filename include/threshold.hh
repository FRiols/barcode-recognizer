#ifndef THRESHOLD_HH_
# define THRESHOLD_HH_

#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;

Mat tophat_with_binarisation(const Mat &tophat, const Mat &bin);
Mat threshold(Mat mtx);
Mat reduce_noise(const Mat &src);

#endif /* !THRESHOLD_HH_ */
