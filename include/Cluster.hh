#ifndef CLUSTER_HH_
# define CLUSTER_HH_

#include <opencv2/opencv.hpp>

using namespace cv;

std::pair<Point, Point> extreme_points(std::vector<Point> contour);
std::pair<Point, Point> extreme_points_bis(std::vector<Point> contour, int w);
std::vector<Point> min_max_points(std::vector<Point> contour);

class Cluster
{
    public:
        Cluster(std::vector<int> ids, std::vector<std::vector<Point> > contours);
        virtual ~Cluster();

        void drawContours(Mat &src, int i, Vec3b color);
        Mat drawRectangle(Mat &src);
        double compute_angle_with_Oxy(std::vector<Point> contour);
        void compute_angle();
        Mat remove_little_angle_occurences(const Mat &src);
		Mat remove_false_barcode(const Mat &src);
        Mat remove_different_colors(const Mat &src);
        std::vector<Point> min_max_cluster(int w);
        Mat disproportional_barcode(const Mat &src);

        int getSize();

        std::vector<int> ids_;
        std::vector<std::vector<Point> > contours_;

    private:
        std::vector<std::pair<std::vector<Point>, int> > same_angle;
        std::vector<int> angles_;
        int angleHist_[180];
};

#endif /* !CLUSTER_HH_ */
