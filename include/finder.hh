#ifndef FINDER_H
# define FINDER_H

#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

cv::Mat finder(cv::Mat mtx);

#endif //FINDER_H
