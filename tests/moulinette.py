#! /usr/bin/python
# -*-coding:Utf-8 -*

#=== IMPORT ==

import timeit
import subprocess
import shlex
import os
import filecmp
import sys
import json
import ast
import operator

#= ! IMPORT ==

#=== CLASSES ==

## This class is only use to display colors in the tests suite to make it friendly.
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

#= ! CLASSES ==

#=== FUNCTIONS ==

def compare(etiquettes, out, image):
    ref_out = etiquettes[image]
    print ref_out + " & " + str(out)
    return ref_out == str(out)

def test_app(image, path, etiquettes):
    FNULL = open(os.devnull, 'w')
    start_time = timeit.default_timer()
    p = subprocess.Popen(['../build/barCodeRecognition', path + image, 'true'], stderr=FNULL, stdout=FNULL)
    temp = p.wait()
    out = p.returncode
    elapsed = timeit.default_timer() - start_time
    FNULL.close()

    if compare(etiquettes, out, image) == True:
        print "[" + bcolors.OKGREEN + "OK" + bcolors.ENDC + "]" + " Image " + image + " recognized well in " + str(elapsed) + " seconds"
        return True
    else:
        print "[" + bcolors.FAIL + "KO" + bcolors.ENDC + "]" + " Image " + image + " not recognized well"
        return False

def fill_args(difficulty, path):
    files = os.listdir(path + difficulty)
    for x in files:
        if x == '.DS_Store':
            continue
        if difficulty == "facile":
            facile.append(x)
        elif difficulty == "moins_facile":
            moins_facile.append(x)
        elif difficulty == "quasi_difficile":
            quasi_difficile.append(x)
        else:
            difficile.append(x)

#= ! FUNCTIONS ==

#=== MAIN ==

## This is the main function. It calls all the test functions.
#order = 1 -> easy then less easy then nearly difficult then difficult
#order = 2 -> the first are the managed, then come the failed
order = 2

delimiter = "=" * 40
print delimiter
print "Begin testing..."

difficulties = ["facile", "moins_facile", "quasi_difficile", "difficile"]
path = "../images/"
#les étiquettes de toutes les images sont à remplir, seules les faciles ont été faites
etiquettes = {'HPIM6404.JPG': '0', 'HPIM6406.JPG': '0', 'HPIM6410.JPG': '0',
'HPIM6413.JPG': '1', 'HPIM6416.JPG': '0', 'HPIM6421.JPG': '1', 'HPIM6423.JPG':
'1', 'HPIM6424.JPG': '1', 'HPIM6425.JPG': '0', 'HPIM6427.JPG': '0',
'HPIM6400.JPG': '0', 'HPIM6402.JPG': '1', 'HPIM6409.JPG': '1', 'HPIM6414.JPG':
'1', 'HPIM6415.JPG': '2', 'HPIM6417.JPG': '1', 'HPIM6418.JPG': '1',
'HPIM6422.JPG': '1', 'HPIM6419.JPG': '2', 'HPIM6396.JPG': '1', 'HPIM6401.JPG': '1', 'HPIM6420.JPG': '1', 'HPIM6412.JPG': '1'}

ey = '../images/facile/'
lsy = '../images/moins_facile/'
alf = '../images/quasi_difficile/'
df = '../images/difficile/'
I1 = 'HPIM6404.JPG' # -- ey
I2 = 'HPIM6406.JPG'
I3 = 'HPIM6410.JPG'
I4 = 'HPIM6413.JPG'
I5 = 'HPIM6416.JPG'
I6 = 'HPIM6421.JPG'
I7 = 'HPIM6423.JPG'
I8 = 'HPIM6424.JPG'
I9 = 'HPIM6425.JPG'
I10 = 'HPIM6427.JPG' # -- ey
I11 = 'HPIM6400.JPG' # -- lsy
I12 = 'HPIM6402.JPG'
I13 = 'HPIM6409.JPG'
I14 = 'HPIM6414.JPG'
I15 = 'HPIM6415.JPG'
I16 = 'HPIM6417.JPG'
I17 = 'HPIM6418.JPG'
I18 = 'HPIM6422.JPG' # -- lsy
I19 = 'HPIM6402.JPG' # -- lsy
I20 = 'HPIM6419.JPG' # -- alf
I21 = 'HPIM6396.JPG' # -- df
I22 = 'HPIM6401.JPG' # -- df
I23 = 'HPIM6420.JPG' # -- df
I24 = 'HPIM6412.JPG' # -- alf

facile = []
moins_facile = []
quasi_difficile = []
difficile = []

for diff in difficulties:
    fill_args(diff, path)

#put all images here like the following example : ['HPIM6406.JPG', 'HPIM64023.JPG'...]
images_order2 = [I1, I18, I2, I24, I8, I23, I20, I12, I7, I16, I21, I9, I22, I11, I3, I6, I10, I4, I15, I17, I13, I5, I14]
#put all paths corresponding to images here like the following example : ['../images/facile/', '../images/facile/']
paths_order2 = [ey, lsy, ey, alf, ey, df, alf, lsy, ey, lsy, df, ey, df, lsy, ey, ey, ey, ey, lsy, lsy, lsy, ey, lsy]

passed = 0.0
testsNmb = 0.0
testsNmb = len(facile) + len(moins_facile) + len(quasi_difficile) + len(difficile)

if order == 1:
    print "---------- Testing easy ----------"
    for img in facile:
        res = test_app(img, path + 'facile/', etiquettes)
        if res == True:
            passed += 1
    print "------------ Easy done -----------"

    print "-------- Testing less easy -------"
    for img in moins_facile:
        res = test_app(img, path + 'moins_facile/', etiquettes)
        if res == True:
            passed += 1
    print "-------- Less easy done ----------"

    print "---- Testing nearly difficult ----"
    for img in quasi_difficile:
        res = test_app(img, path + 'quasi_difficile/', etiquettes)
        if res == True:
            passed += 1
    print "----- Nearly difficult done ------"

    print "------- Testing difficult --------"
    for img in difficile:
        res = test_app(img, path + 'difficile/', etiquettes)
        if res == True:
            passed += 1
    print "--------- Difficult done ---------"
else:
    print "----------------------------------"
    i = 0
    while i < len(images_order2):
        res = test_app(images_order2[i], paths_order2[i], etiquettes)
        if res == True:
            passed += 1
        i += 1
    print "----------------------------------"

percentage = (passed / testsNmb) * 100
print "---> " + bcolors.HEADER + str(passed) + " passed tests out of " + str(testsNmb) + " (" + str(percentage) + "%)." + bcolors.ENDC

print "Testing done."
print delimiter

#= ! MAIN ==
