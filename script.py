import os
from subprocess import call

for dirname, dirnames, filenames in os.walk('./pictures/'):
    for filename in filenames:
        f = os.path.join(dirname, filename)
        print(os.path.join(dirname, filename))
        call(["./build/barCodeRecognition", f])
