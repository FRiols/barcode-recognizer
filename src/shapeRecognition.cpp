#include "../include/shapeRecognition.hh"

ShapeRecognition::ShapeRecognition()
{
}

ShapeRecognition::~ShapeRecognition()
{
}

void ShapeRecognition::drawContours(Mat &src, int i, Vec3b color)
{
    std::vector<Point> contour = ctrs_ids.at(i).first;
    for (int j = 0; j < contour.size(); ++j)
    {
        Point pt = contour.at(j);
        src.at<Vec3b>(pt) = color;
    }
}

std::vector<std::vector<int> > ShapeRecognition::merge_clusters(
        std::vector<std::vector<int> > clusters)
{
    std::vector<std::vector<int> > final_cl;

    for (int i = 0; i < clusters.size(); ++i)
    {
        if (clusters.at(i).size() == 0)
            continue;

        std::vector<int> cluster = clusters.at(i);


        for (int j = 0; j < clusters.at(i).size(); ++j)
        {
            int id = clusters.at(i).at(j);

            int c;
            std::vector<int>::iterator exists;

            for (c = i + 1; c < clusters.size(); ++c)
            {
                exists = find(clusters.at(c).begin(), clusters.at(c).end(), id);
                if (exists != clusters.at(c).end())
                    break;
            }
            if (c != clusters.size())
            {
                for (int k = 0; k < clusters.at(c).size(); ++k)
                {
                    bool isIn = false;
                    for (int p = 0; p < cluster.size(); ++p)
                    {
                        if (clusters.at(c).at(k) == cluster.at(p))
                        {
                            isIn = true;
                            break;
                        }
                    }
                    if (isIn == false)
                        cluster.push_back(clusters.at(c).at(k));
                }
                clusters.at(c).clear();
            }
        }
        final_cl.push_back(cluster);
    }
    return final_cl;
}

std::vector<std::vector<int> > ShapeRecognition::remove_little_clusters(
        std::vector<std::vector<int> > clusters)
{
    std::vector<std::vector<int> > final_cl;
    for (int i = 0; i < clusters.size(); ++i)
    {
        if (clusters.at(i).size() > 2)
            final_cl.push_back(clusters.at(i));
    }

    std::vector<std::pair<std::vector<Point>, int> > ctrs_ids_tmp;
    for (int i = 0; i < final_cl.size(); ++i)
    {
        for (int k = 0; k < final_cl.at(i).size(); ++k)
        {
            int j;
            for (j = 0; j < ctrs_ids.size(); ++j)
            {
                if (ctrs_ids.at(j).second == final_cl.at(i).at(k))
                    break;
            }
            ctrs_ids_tmp.push_back(ctrs_ids.at(j));
        }
    }

    ctrs_ids = ctrs_ids_tmp;

    return final_cl;
}

Mat ShapeRecognition::clusters_colored(const Mat &src,
        std::vector<std::vector<int> > clusters)
{
    Mat dst = cv::Mat::zeros(src.size(), CV_8UC3);
    int r;
    int g;
    int b;
    Vec3b color;

    for (int i = 0; i < clusters.size(); ++i)
    {
        if (clusters.at(i).size() <= 2)
            continue;

        r = rand() % 256;
        g = rand() % 256;
        b = rand() % 256;
        color = Vec3b(b, g, r);

        for (int j = 0; j < clusters.at(i).size(); ++j)
        {
            int id = clusters.at(i).at(j);

            for (int k = 0; k < ctrs_ids.size(); ++k)
            {
                if (ctrs_ids.at(k).second == id)
                {
                    drawContours(dst, k, color);
                    break;
                }
            }
        }
    }

    return dst;
}

Mat ShapeRecognition::make_clusters(const Mat &src)
{
	std::cout << "-- Make Clusters..." << std::endl;
    Mat dst = cv::Mat::zeros(src.size(), CV_8UC3);

    int r = rand() % 256;
    int g = rand() % 256;
    int b = rand() % 256;
    Vec3b color = Vec3b(b, g, r);

    std::vector<int> ids_colored;

    bool new_cluster = true;
    std::vector<int> cluster;

    for (int i = 0; i < ctrs_ids.size(); ++i)
    {
        cluster.clear();

        int c;
        std::vector<int>::iterator exists;
        for (c = 0; c < clusters.size(); ++c)
        {
            exists = find(clusters.at(c).begin(), clusters.at(c).end(),
                    ctrs_ids.at(i).second);
            if (exists != clusters.at(c).end())
                break;
        }

        if (c != clusters.size())
        {
            cluster = clusters.at(c);
            clusters.at(c).clear();
        }
        else
            cluster.push_back(ctrs_ids.at(i).second);

        std::pair<Point, Point> pts = extreme_points(ctrs_ids.at(i).first);
        Point middle(pts.first.x + (pts.second.x - pts.first.x) / 2,
                pts.first.y + (pts.second.y - pts.first.y) / 2);
        int l1 = pts.second.x - pts.first.x;
        int l2 = pts.second.y - pts.first.y;
        int ray2 = pow(pts.second.x - middle.x, 2)
            + pow(pts.second.y - middle.y, 2) + 400;

        for (int j = middle.x - max(l1, l2) / 2;
                j < middle.x + max(l1, l2) / 2; ++j)
        {
            for (int k = middle.y - max(l1, l2) / 2;
                    k < middle.y + max(l1, l2) / 2; ++k)
            {
                // Vérifie que l'on ne soit pas hors image
                if (k < 0 || j < 0
                        || j >= src.cols || k >= src.rows
                        || pow(middle.x - j, 2) + pow(middle.y - k, 2) > ray2)
                    continue;

                // Vérifie si on touche un contour
                if (src.at<Vec3b>(k, j) != Vec3b(0, 0, 0))
                {
                    // Et vérifie que ce n'est pas de lui-même
                    std::vector<Point>::iterator it;
                    it = find(ctrs_ids.at(i).first.begin(),
                            ctrs_ids.at(i).first.end(), Point(j, k));

                    if (it != ctrs_ids.at(i).first.end())
                        break;

                    // Trouve le contour dont il est proche
                    int z;
                    for (z = 0; z < ctrs_ids.size(); ++z)
                    {
                        it = find(ctrs_ids.at(z).first.begin(),
                                ctrs_ids.at(z).first.end(), Point(j, k));
                        if (it != ctrs_ids.at(z).first.end())
                            break;
                    }

                    std::vector<int>::iterator it3;
                    it3 = find(cluster.begin(), cluster.end(),
                            ctrs_ids.at(z).second);
                    if (it3 != cluster.end())
                        break;

                    drawContours(dst, z, color);
                    ids_colored.push_back(ctrs_ids.at(z).second);
                    cluster.push_back(ctrs_ids.at(z).second);
                }
            }
        }

        clusters.push_back(cluster);
        clusters = merge_clusters(clusters);
    }


    clusters = remove_little_clusters(clusters);

    for (int i = 0; i < clusters.size(); ++i)
    {
        std::vector<int> ids_ = clusters.at(i);
        std::vector<std::vector<Point> > contours_;
        for (int j = 0; j < ids_.size(); ++j)
        {
            int k;
            for (k = 0; k < ctrs_ids.size(); ++k)
            {
                if (ctrs_ids.at(k).second == ids_.at(j))
                    break;
            }
            contours_.push_back(ctrs_ids.at(k).first);
        }

        Cluster cl(ids_, contours_);
        Clusters.push_back(cl);
    }

    dst = clusters_colored(src, clusters);
    std::cout << "---- " << Clusters.size() << " clusters" << std::endl;
	std::cout << "-- Clusters done" << std::endl << std::endl;
    return dst;
}

Mat ShapeRecognition::remove_useless_contours(const Mat &src)
{
    Mat dst = cv::Mat::zeros(src.size(), CV_8UC3);

    std::vector<std::pair<std::vector<Point>, int> > c;

    for (int i = 0; i < ctrs_ids.size(); ++i)
    {
        std::pair<Point, Point> pts = extreme_points(ctrs_ids.at(i).first);
        Point middle(pts.first.x + (pts.second.x - pts.first.x) / 2,
                pts.first.y + (pts.second.y - pts.first.y) / 2);
        int l1 = pts.second.x - pts.first.x;
        int l2 = pts.second.y - pts.first.y;

        int ray2 = pow(pts.second.x - middle.x, 2)
            + pow(pts.second.y - middle.y, 2);

        bool removeContours = true;

        for (int j = middle.x - max(l1, l2) / 2;
                j < middle.x + max(l1, l2) / 2; ++j)
        {
            for (int k = middle.y - max(l1, l2) / 2;
                    k < middle.y + max(l1, l2) / 2; ++k)
            {
                if (k < 0 || j < 0
                        || j >= src.cols || k >= src.rows
                        || pow(middle.x - j, 2) + pow(middle.y - k, 2) > ray2)
                    continue;


                if (src.at<Vec3b>(k, j) != Vec3b(0, 0, 0))
                {
                    std::vector<Point>::iterator it;
                    it = find(ctrs_ids.at(i).first.begin(),
                            ctrs_ids.at(i).first.end(), Point(j, k));
                    if (it == ctrs_ids.at(i).first.end())
                        removeContours = false;
                }
            }
        }

        if (removeContours == false)
        {
            drawContours(dst, i, Vec3b(255, 0, 0));
            c.push_back(ctrs_ids.at(i));
        }
        else
            drawContours(dst, i, Vec3b(0, 0, 0));

        //imshow("test", dst);
        //waitKey(0);

    }

    ctrs_ids = c;

    return dst;
}

int ShapeRecognition::euclidean_distance(Point A, Point B)
{
    int res = 0;
    res = sqrt(pow(A.x - B.x, 2) + pow(A.y - B.y, 2));

    return res;
}

Mat ShapeRecognition::big_contours_recognition(const Mat &src)
{
    std::cout << "-- Contours recognition..." << std::endl;
    int height = src.rows;
    int width = src.cols;

    Mat dst = cv::Mat::zeros(src.size(), CV_8UC3);

    std::vector<std::vector<Point> > c;
    findContours(src, c, CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE);

    std::vector<std::pair<std::vector<Point>, int> > c_ids;
    for (int i = 0; i < c.size(); ++i)
    {
        std::pair<std::vector<Point>, int> c_id;
        c_id.first = c.at(i);
        c_id.second = i;
        c_ids.push_back(c_id);
    }

    Vec3b black(0, 0, 0);
    Vec3b blue(255, 0, 0);
    Vec3b green(0, 255, 0);
    Vec3b red(0, 0, 255);
    Vec3b yellow(255, 255, 0);
    Vec3b purple(255, 0, 255);

    for (int i = 0; i < c.size(); ++i)
    {
        std::vector<Point> pts = min_max_points(c.at(i));

        int ab = euclidean_distance(pts.at(0), pts.at(1));
        int bc = euclidean_distance(pts.at(1), pts.at(2));
        int cd = euclidean_distance(pts.at(2), pts.at(3));
        int da = euclidean_distance(pts.at(3), pts.at(0));

        int L = max(max(ab, max(bc, cd)), da);
        int l = min(min(ab, min(bc, cd)), da);

        int L2 = 0;
        int l2 = 0;

        if (L == ab)
            L2 = cd;
        else if (L == bc)
            L2 = da;
        else if (L == cd)
            L2 = ab;
        else if (L == da)
            L2 = bc;

        if (l == ab)
            l2 = cd;
        else if (l == bc)
            l2 = da;
        else if (l == cd)
            l2 = ab;
        else if (l == da)
            l2 = bc;

        int tmp = L2;
        L2 = max(l2, L2);
        l2 = min(l2, tmp);

        std::pair<Point, Point> ext_pts = extreme_points(c_ids.at(i).first);
        int ext_l1 = ext_pts.second.x - ext_pts.first.x;
        int ext_l2 = ext_pts.second.y - ext_pts.first.y;

        bool vertical = false;
        bool horizontal = false;
        bool diagonal = false;

        if (ext_l1 > ext_l2 * 5)
            horizontal = true;
        else if (ext_l2 > ext_l1 * 5)
            vertical = true;
        else
            diagonal = true;

        if (c.at(i).size() <= 10 || max(ext_l1, ext_l2) <= 10)
        {
        }
        else if ((horizontal && max(ext_l1, ext_l2) > width / 4)
            || (vertical && max(ext_l1, ext_l2) > height / 4))
        {
        }
        else if (!diagonal && max(ext_l1, ext_l2) >= 5
                && max(ext_l1, ext_l2) > 3 * min(ext_l1, ext_l2))
        {
            ctrs_ids.push_back(c_ids.at(i));
            drawContours(dst, ctrs_ids.size() - 1, blue);
        }
        else if (!diagonal)
        {
        }
        else if (L > sqrt(pow(height, 2) + pow(width, 2)) / 4)
        {
        }
        else if (l + 15 < l2 || L - 15 > L2 || max(ext_l1, ext_l2) <= 15)
        {
        }
        else if (L < l * 3)
        {
        }
        else
        {
            ctrs_ids.push_back(c_ids.at(i));
            drawContours(dst, ctrs_ids.size() - 1, blue);
        }
    }

    std::cout << "-- Contours recognition done" << std::endl << std::endl;

    return dst;
}

std::vector<std::vector<int> > ShapeRecognition::getClusters()
{
    return clusters;
}
