#include "../include/Cluster.hh"
#include "../include/main.hh"

/* CONSTRUCTOR & DESTRUCTOR */
/* -------------------------------- */

Cluster::Cluster(std::vector<int> ids, std::vector<std::vector<Point> > contours)
{
    ids_ = ids;
    contours_ = contours;
}

Cluster::~Cluster()
{
}


/* DRAW */
/* -------------------------------- */

void Cluster::drawContours(Mat &src, int i, Vec3b color)
{
    std::vector<Point> contour = contours_.at(i);
    for (int j = 0; j < contour.size(); ++j)
    {
        Point pt = contour.at(j);
        src.at<Vec3b>(pt) = color;
    }
}

Mat Cluster::drawRectangle(Mat &src)
{
    int width = src.cols;
    Mat result(src);

    std::vector<Point> P = min_max_cluster(width);

    line(result, P.at(0), P.at(1), Scalar(255, 0, 0), 4);
    line(result, P.at(2), P.at(1), Scalar(255, 0, 0), 4);
    line(result, P.at(2), P.at(3), Scalar(255, 0, 0), 4);
    line(result, P.at(0), P.at(3), Scalar(255, 0, 0), 4);

    line(result, P.at(0), P.at(2), Scalar(0, 255, 0), 4);
    line(result, P.at(1), P.at(3), Scalar(0, 255, 0), 4);

    return result;
}

/* MAIN FUNCTIONS */
/* -------------------------------- */

Mat Cluster::disproportional_barcode(const Mat &src)
{
    int heigth = src.rows;
    int width = src.cols;
    Mat dst(src);

    std::vector<Point> P = min_max_cluster(width);

    int ab = sqrt(pow(P.at(1).x - P.at(0).x, 2)
            + pow(P.at(1).y - P.at(0).y, 2));
    int bc = sqrt(pow(P.at(2).x - P.at(1).x, 2)
            + pow(P.at(2).y - P.at(1).y, 2));
    int cd = sqrt(pow(P.at(3).x - P.at(2).x, 2)
            + pow(P.at(3).y - P.at(2).y, 2));
    int da = sqrt(pow(P.at(0).x - P.at(3).x, 2)
            + pow(P.at(0).y - P.at(3).y, 2));

    if (max(ab, cd) >= 1.5 * min(ab, cd)
        || max(bc, da) >= 1.5 * min(bc, da)
        || min(min(ab, bc), min(cd, da)) * 3.2 < max(max(ab, bc), max(cd, da))
        || min(min(ab, bc), min(cd, da)) * 1.3 > max(max(ab, bc), max(cd, da)))
    {
        for (int i = 0; i < ids_.size(); ++i)
            drawContours(dst, i, Vec3b(0, 0, 0));

        ids_.clear();
        angles_.clear();
        contours_.clear();
    }

    return dst;
}

double Cluster::compute_angle_with_Oxy(std::vector<Point> contour)
{
    std::pair<Point, Point> ext_pts = extreme_points(contour);
    std::vector<Point> pts = min_max_points(contour);
    if (pts.at(0).y > pts.at(2).y)
    {
        Point tmp = ext_pts.first;
        ext_pts.first.x = ext_pts.second.x;
        ext_pts.second.x = tmp.x;
    }

    double a = -((double)ext_pts.second.y - (double)ext_pts.first.y)
        / ((double)ext_pts.second.x - (double)ext_pts.first.x);

    double tanPhi = sqrt(pow(a, 2));
    double res = (double)(atan(tanPhi) * 180 / CV_PI);

    if (a < 0)
        return 180 - res;

    return res;
}

void Cluster::compute_angle()
{
    angles_.clear();
    for (int i = 0; i < 180; ++i)
        angleHist_[i] = 0;

    for (int i = 0; i < ids_.size(); ++i)
    {
        int angle = compute_angle_with_Oxy(contours_.at(i));
        ++angleHist_[angle];
        angles_.push_back(angle);
    }
}

Mat Cluster::remove_little_angle_occurences(const Mat &src)
{
    Mat dst(src);

    int maxAngle = 0;
    int maxIt = 0;

    int maxAngle2 = 0;
    int maxIt2 = 0;

    for (int i = 0; i < 180; ++i)
    {
        if (angleHist_[i] > maxIt)
        {
            maxAngle2 = maxAngle;
            maxIt2 = angleHist_[maxAngle2];
            maxAngle = i;
            maxIt = angleHist_[i];
        }
    }


    if (maxAngle != 0
            && (maxAngle + 2 >= maxAngle2 || maxAngle - 2 <= maxAngle2))
        maxIt = maxIt + angleHist_[maxAngle + 1] + angleHist_[maxAngle + 2]
                 + angleHist_[maxAngle - 1] + angleHist_[maxAngle - 2];


    if (maxIt <= 2)
    {
        for (int i = 0; i < ids_.size(); ++i)
            drawContours(dst, i, Vec3b(0, 0, 0));

        ids_.clear();
        contours_.clear();
        angles_.clear();
        return dst;
    }

    while (true)
    {
        std::vector<int> id;
        std::vector<int> ang;
        std::vector<std::vector<Point> > cont;

        int mean = 0;
        int mi = 180;
        int ma = 0;
        int minIt = 0;
        int maxIt = 0;
        for (int i = 0; i < ids_.size(); ++i)
        {
            if (angles_.at(i) < mi)
            {
                mi = angles_.at(i);
                minIt = i;
            }
            if (angles_.at(i) > ma)
            {
                ma = angles_.at(i);
                maxIt = i;
            }

            std::vector<Point> pts = min_max_points(contours_.at(i));
        }
        mean = maxAngle;

        bool remMax = false;
        bool remMin = false;
        if (mean <= 15 && ma > 165)
        {
            int diff = 180 - ma + mean;
            if (diff > 15)
                remMax = true;
        }
        else if (mean + 15 < ma)
            remMax = true;

        if (mean >= 165 && mi < 15)
        {
            int diff = 180 - mean + mi;
            if (diff > 15)
                remMin = true;
        }
        else if (mean - 15 > mi)
            remMin = true;

        if (remMax == false && remMin == false)
            break;

        for (int i = 0; i < ids_.size(); ++i)
        {
            if ((remMin == true && i == minIt)
                    || (remMax == true && i == maxIt))
            {
                drawContours(dst, i, Vec3b(0, 0, 0));
                continue;
            }

            id.push_back(ids_.at(i));
            ang.push_back(angles_.at(i));
            cont.push_back(contours_.at(i));
        }

        ids_ = id;
        angles_ = ang;
        contours_ = cont;
    }

    return dst;
}

Mat Cluster::remove_false_barcode(const Mat &src)
{
    Mat dst(src);
    int w = src.cols;

    int hor = 0;
    int ver = 0;
    int diag = 0;

    std::vector<int> diagonals;
    std::vector<int> horizontals;
    std::vector<int> verticals;

    int distS = 1000000000;
    int distL = 0;
    Point minP(1000000, 1000000);
    Point maxP(0, 0);

    int meanH = 0;
    int meanV = 0;

    for (int i = 0; i < ids_.size(); ++i)
    {
        std::pair<Point, Point> ext_pts = extreme_points(contours_.at(i));
        std::pair<Point, Point> ext_pts2 = extreme_points_bis(contours_.at(i),
                w);
        int ext_l1 = max(ext_pts.second.x, ext_pts2.second.x)
                        - min(ext_pts.first.x, ext_pts2.first.x);
        int ext_l2 = max(ext_pts.second.y, ext_pts2.second.y)
                        - min(ext_pts.first.y, ext_pts2.first.y);

        int a = sqrt(pow(min(ext_pts.first.x, ext_pts2.first.x), 2)
                + pow(min(ext_pts.first.y, ext_pts2.first.y), 2));
        int b = sqrt(pow(max(ext_pts.second.x, ext_pts2.second.x), 2)
                + pow(max(ext_pts.second.y, ext_pts2.second.y), 2));
        distS = min(distS, a);
        distL = max(distL, b);

        if (distS == a)
        {
            minP.x = min(ext_pts.first.x, ext_pts2.first.x);
            minP.y = min(ext_pts.first.y, ext_pts2.first.y);
        }
        if (distL == b)
        {
            maxP.x = max(ext_pts.second.x, ext_pts2.second.x);
            maxP.y = max(ext_pts.second.y, ext_pts2.second.y);
        }

        if (ext_l1 > ext_l2 * 5)
        {
            horizontals.push_back(ids_.at(i));
            meanH += ext_l1;
            ++hor;
        }
        else if (ext_l2 > ext_l1 * 5)
        {
            verticals.push_back(ids_.at(i));
            meanV += ext_l2;
            ++ver;
        }
        else
        {
            diagonals.push_back(ids_.at(i));
            ++diag;
        }
    }

    if (hor != 0)
        meanH = meanH / hor;
    if (ver != 0)
        meanV = meanV / ver;

    int ma = max(hor, max(ver, diag));
    int mid = 0;
    int hvd = 0;
    if (ma == hor)
    {
        mid = max(ver, diag);
        hvd = 0;
    }
    else if (ma == ver)
    {
        mid = max(diag, hor);
        hvd = 1;
    }
    else if (ma == diag)
    {
        mid = max(hor, ver);
        hvd = 2;
    }

    int heigth = src.rows;
    int width = src.cols;

    if ((maxP.y - minP.y > heigth / 1.6) || (maxP.x - minP.x > width / 1.6))
    {
        for (int i = 0; i < ids_.size(); ++i)
            drawContours(dst, i, Vec3b(0, 0, 0));

        ids_.clear();
        contours_.clear();
        angles_.clear();
        return dst;
    }

    if (ma == hor && meanH * 2 > 2.8 * (maxP.y - minP.y))
    {
        for (int i = 0; i < ids_.size(); ++i)
            drawContours(dst, i, Vec3b(0, 0, 0));

        ids_.clear();
        contours_.clear();
        angles_.clear();
        return dst;
    }

    if (ma == ver && meanV * 2 > 2.8 * (maxP.x - minP.x))
    {
        for (int i = 0; i < ids_.size(); ++i)
            drawContours(dst, i, Vec3b(0, 0, 0));

        ids_.clear();
        contours_.clear();
        angles_.clear();
        return dst;
    }

    std::vector<int> id;
    std::vector<int> ang;
    std::vector<std::vector<Point> > cont;

    if (ma > 3 * mid)
    {
        for (int i = 0; i < ids_.size(); ++i)
        {
            int hvdi = -1;
            // Is horizontal
            for (int j = 0; j < horizontals.size(); ++j)
            {
                if (horizontals.at(j) == ids_.at(i))
                {
                    hvdi = 0;
                    break;
                }
            }

            // Is vertical
            for (int j = 0; j < verticals.size(); ++j)
            {
                if (hvdi != -1)
                    break;
                if (verticals.at(j) == ids_.at(i))
                {
                    hvdi = 1;
                    break;
                }
            }

            // Is diagonal
            for (int j = 0; j < diagonals.size(); ++j)
            {
                if (hvdi != -1)
                    break;
                if (diagonals.at(j) == ids_.at(i))
                {
                    hvdi = 2;
                    break;
                }
            }

            if (hvdi != hvd)
            {
                drawContours(dst, i, Vec3b(0, 0, 0));
                continue;
            }

            id.push_back(ids_.at(i));
            ang.push_back(angles_.at(i));
            cont.push_back(contours_.at(i));
        }

        ids_ = id;
        angles_ = ang;
        contours_ = cont;
    }


    return dst;
}

Mat Cluster::remove_different_colors(const Mat &src)
{
    Mat dst(src);

    int maxAngle = 0;
    int maxIt = 0;
    std::vector<Vec3b> colors;

    for (int i = 0; i < angles_.size(); ++i)
    {
        maxAngle = i;
        maxIt = angles_.at(i);
        Vec3b mean(0, 0, 0);
        int r = 0; int g = 0; int b = 0;

        for (int j = 0; j < contours_.at(i).size(); ++j)
        {
            Vec3b tmp = src.at<Vec3b>(contours_.at(i).at(j));
            b += tmp[0];
            g += tmp[1];
            r += tmp[2];
        }

        mean[0] = b / contours_.at(i).size();
        mean[1] = g / contours_.at(i).size();
        mean[2] = r / contours_.at(i).size();
        colors.push_back(mean);
    }

    return dst;
}

/* Compute extreme points */
/* -------------------------------- */

std::vector<Point> Cluster::min_max_cluster(int w)
{
    std::vector<Point> pts;
    Point A(1000000, 1000000); // En haut à gauche
    Point B; // En haut à droite
    Point C(0, 0); // En bas à droite
    Point D; // En bas à gauche

    int distA = 1000000000;
    int distC = 0;
    int distB = 1000000000;
    int distD = 0;

    for (int i = 0; i < ids_.size(); ++i)
    {
        std::pair<Point, Point> ext_pts = extreme_points(contours_.at(i));

        int a = sqrt(pow(ext_pts.first.x, 2) + pow(ext_pts.first.y, 2));
        int c = sqrt(pow(ext_pts.second.x, 2) + pow(ext_pts.second.y, 2));
        distA = min(distA, a);
        distC = max(distC, c);

        if (distA == a)
            A = ext_pts.first;
        if (distC == c)
            C = ext_pts.second;


        std::pair<Point, Point> ext_pts2 = extreme_points_bis(contours_.at(i),
                                                                w);

        int b = sqrt(pow(w - ext_pts2.first.x, 2) + pow(ext_pts2.first.y, 2));
        int d = sqrt(pow(w - ext_pts2.second.x, 2) + pow(ext_pts2.second.y, 2));
        distB = min(distB, b);
        distD = max(distD, d);

        if (distB == b)
            B = ext_pts2.first;
        if (distD == d)
            D = ext_pts2.second;

    }




    pts.push_back(A);
    pts.push_back(B);
    pts.push_back(C);
    pts.push_back(D);

    return pts;
}

std::pair<Point, Point> extreme_points(std::vector<Point> contour)
{
    std::pair<Point, Point> pts;

    int distS = 1000000000;
    int distL = 0;
    Point minP(0, 0);
    Point maxP(0, 0);

    for (int i = 0; i < contour.size(); ++i)
    {
        Point pi = contour.at(i);
        int dist = sqrt(pow(pi.x, 2) + pow(pi.y, 2));

        distS = min(distS, dist);
        distL = max(distL, dist);

        if (distS == dist)
            minP = pi;
        if (distL == dist)
            maxP = pi;
    }

    pts.first = minP;
    pts.second = maxP;

    return pts;
}

std::pair<Point, Point> extreme_points_bis(std::vector<Point> contour, int w)
{
    std::pair<Point, Point> pts;





    int distS = 1000000000;
    int distL = 0;
    Point minP(0, 0);
    Point maxP(0, 0);

    for (int i = 0; i < contour.size(); ++i)
    {
        Point pi = contour.at(i);
        int dist = sqrt(pow(w - pi.x, 2) + pow(pi.y, 2));

        distS = min(distS, dist);
        distL = max(distL, dist);

        if (distS == dist)
            minP = pi;
        if (distL == dist)
            maxP = pi;
    }

    pts.first = minP;
    pts.second = maxP;



/*    int distB = 0;
    int distD = 0;
    Point B(0, 1000000);
    Point D(1000000, 0);

    for (int i = 0; i < contour.size(); ++i)
    {
        Point pi = contour.at(i);
        int dist = sqrt(pow(pi.x, 2) + pow(pi.y, 2));

        if (B.x < pi.x && B.y > pi.y)
        {
            B = pi;
        }
        if (D.x > pi.x && D.y < pi.y)
        {
            D = pi;
        }

    }

    pts.first = B;
    pts.second = D;*/

    return pts;
}


std::vector<Point> min_max_points(std::vector<Point> contour)
{
    std::vector<Point> pts;
    Point A(10000, 0);  // min i
    Point B(0, 10000);  // min j
    Point C(0, 0);      // max i
    Point D(0, 0);      // max j

    for (int j = 0; j < contour.size(); ++j)
    {
        Point tmp = contour.at(j);
        if (tmp.x < A.x)
            A = tmp;
        if (tmp.y < B.y)
            B = tmp;
        if (tmp.x > C.x)
            C = tmp;
        if (tmp.y > D.y)
            D = tmp;
    }

    pts.push_back(A);
    pts.push_back(B);
    pts.push_back(C);
    pts.push_back(D);

    return pts;
}

int Cluster::getSize()
{
    return ids_.size();
}
