#include "../include/threshold.hh"

using namespace cv;

Mat tophat_with_binarisation(const Mat &tophat, const Mat &bin)
{
    std::cout << "-- Combo tophat + binarisation..." << std::endl;
    int heigth = tophat.rows;
    int width = tophat.cols;

    Mat result(Size(width, heigth), tophat.type());

    for (int i = 0; i < width; ++i)
    {
        for (int j = 0; j < heigth; ++j)
        {
            if (tophat.at<uchar>(j, i) < 20)
                result.at<uchar>(j, i) = 255;
            else
                result.at<uchar>(j, i) = bin.at<uchar>(j, i);
        }
    }

    std::cout << "-- Combo tophat + binarisation done" << std::endl << std::endl;

    return result;
}

Mat reduce_noise(const Mat &src)
{
    int heigth = src.rows;
    int width = src.cols;

    Mat result(Size(width, heigth), src.type());

    for (int i = 0; i < width; ++i)
    {
        for (int j = 0; j < heigth; ++j)
        {
            int num = 0;
            uchar pix = src.at<uchar>(j, i);

            for (int k = i - 1; k <= i + 1; ++k)
                for (int l = j - 1; l <= j + 1; ++l)
                {
                    if (k < 0 || l < 0)
                        continue;
                    if (k > width || l > heigth)
                        continue;

                    if (src.at<uchar>(l, k) != pix)
                        ++num;
                }

            if (num >= 7 && pix == 255)
                result.at<uchar>(j, i) = 0;
            else if (num >= 7 && pix == 0)
                result.at<uchar>(j, i) = 255;
            else
                result.at<uchar>(j, i) = pix;
        }
    }

    return result;
}

Mat threshold(Mat mtx)
{
    std::cout << "-- Binarisation Niblack..." << std::endl;
    int heigth = mtx.rows;
    int width = mtx.cols;
    int largeur = 20; // La largeur de la fenêtre de calcul
    int longueur = 20; // La longueur de la fenêtre de calcul
    int debut_abs = 0;
    int debut_ord = 0;
    int fin_abs = 0;
    int fin_ord = 0;

    bool display = false;

    Mat result(Size(width, heigth), mtx.type());

    for (int i = 0; i < width; ++i)
    {
        for (int j = 0; j < heigth; ++j)
        {
            if (i < longueur / 2)
                debut_ord = 0;
            else
                debut_ord = i - longueur / 2;

            if (i > (width - longueur / 2))
                fin_ord = width;
            else
                fin_ord = i + longueur / 2;

            if (j < largeur / 2)
                debut_abs = 0;
            else
                debut_abs = j - largeur / 2;

            if (j > (heigth - largeur / 2))
                fin_abs = heigth;
            else
                fin_abs = j + largeur / 2;

            double somme = 0;
            double constante = 0.8;
            double ecart_type = 0;
            double *pixels = (double*)malloc(sizeof (double)
                                * ((fin_abs - debut_abs)
                                * (fin_ord - debut_ord)));

            int indice = 0;
            for (int k = debut_abs; k < fin_abs; ++k)
            {
                for (int l = debut_ord; l < fin_ord; ++l)
                {
                    somme += mtx.at<uchar>(k, l);
                    pixels[indice] = mtx.at<uchar>(k, l);
                    indice++;
                }
            }
            double moyenne = somme / ((fin_abs - debut_abs) * (fin_ord - debut_ord));
            double variance = 0;
            for (int k = 0; k < indice; ++k)
                variance += pixels[k] * pixels[k];
            free(pixels);
            variance = variance - moyenne * moyenne;
            variance = variance / ((fin_abs - debut_abs) * (fin_ord - debut_ord));
            ecart_type = sqrt(variance);
            double seuil = moyenne * (1 + constante * ((ecart_type / 256) - 1));
            if (mtx.at<uchar>(j, i) >= seuil * 1.5) // -> blanc
                result.at<uchar>(j, i) = 255;
            else // -> noir
                result.at<uchar>(j, i) = 0;
        }
    }

    std::cout << "-- Binarisation done" << std::endl << std::endl;
    return result;
}
