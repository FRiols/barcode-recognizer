#include "../include/finder.hh"
#include <math.h>

using namespace cv;

Mat finder(Mat mtx)
{
    std::cout << "-- Finding..." << std::endl;

    bool debug = false;

    int heigth = mtx.rows;
    int width = mtx.cols;

    Mat result(Size(width, heigth), mtx.type());

    int **x_filter = new int*();
    for (int i = 0; i < 3; ++i)
        x_filter[i] = new int(3);

    int **y_filter = new int*();
    for (int i = 0; i < 3; ++i)
        y_filter[i] = new int(3);

    x_filter[0][0] = -1;
    x_filter[0][1] = 0;
    x_filter[0][2] = 1;
    x_filter[1][0] = -2;
    x_filter[1][1] = 0;
    x_filter[1][2] = 2;
    x_filter[2][0] = -1;
    x_filter[2][1] = 0;
    x_filter[2][2] = 1;

    y_filter[0][0] = -1;
    y_filter[0][1] = -2;
    y_filter[0][2] = -1;
    y_filter[1][0] = 0;
    y_filter[1][1] = 0;
    y_filter[1][2] = 0;
    y_filter[2][0] = 1;
    y_filter[2][1] = 2;
    y_filter[2][2] = 1;

    if (debug)
    {
        std::cout << "Avant la boucle sur les pixels" << std::endl;
        std::cout << "width : " << width << " ; heigth : " << heigth << std::endl;
    }

    for (int i = 1; i < width - 1; ++i)
    {
        for (int j = 1; j < heigth - 1; ++j)
        {
            uchar pixel_x = (x_filter[0][0] * mtx.at<uchar>(j - 1, i - 1) + x_filter[0][1] * mtx.at<uchar>(j, i - 1) + x_filter[0][2] * mtx.at<uchar>(j + 1, i - 1) +
                    x_filter[1][0] * mtx.at<uchar>(j - 1, i) + x_filter[1][1] * mtx.at<uchar>(j, i) + x_filter[1][2] * mtx.at<uchar>(j + 1, i) +
                    x_filter[2][0] * mtx.at<uchar>(j - 1, i + 1) + x_filter[2][1] * mtx.at<uchar>(j, i + 1) + x_filter[2][2] * mtx.at<uchar>(j + 1, i + 1));

            uchar pixel_y = (y_filter[0][0] * mtx.at<uchar>(j - 1, i - 1) + y_filter[0][1] * mtx.at<uchar>(j, i - 1) + y_filter[0][2] * mtx.at<uchar>(j + 1, i - 1) +
                    y_filter[1][0] * mtx.at<uchar>(j - 1, i) + y_filter[1][1] * mtx.at<uchar>(j, i) + y_filter[1][2] * mtx.at<uchar>(j + 1, i) +
                    y_filter[2][0] * mtx.at<uchar>(j - 1, i + 1) + y_filter[2][1] * mtx.at<uchar>(j, i + 1) + y_filter[2][2] * mtx.at<uchar>(j + 1, i + 1));

            double value = sqrt((pixel_x * pixel_x) * (pixel_y * pixel_y));

            result.at<uchar>(j, i) = value;
        }
    }

    std::cout << "---- Finding done" << std::endl;
    return result;
}
