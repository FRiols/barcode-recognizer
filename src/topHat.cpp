#include "../include/topHat.hh"

Mat reduce_num_colors(Mat &src)
{
    int heigth = src.rows;
    int width = src.cols;

    for (int i = 0; i < width; ++i)
        for (int j = 0; j < heigth; ++j)
		{
			uchar diff = src.at<uchar>(j, i) % 30;
			src.at<uchar>(j, i) = src.at<uchar>(j, i) - diff;
		}

	return src;
}

Mat dilate_image(const Mat &src)
{
    std::cout << "------ Dilation..." << std::endl;
    int kernel_size = 6;
    int i, j, k1, k2;
    int heigth = src.rows;
    int width = src.cols;
    Mat dst(Size(width, heigth), src.type());

    uchar max = 0;
    int imax = 0;
    int jmax = 0;

# pragma omp parallel for collapse(3)
    for (i = 0; i < width; ++i)
    {
        imax = 0;
        jmax = 0;
        for (j = 0; j < heigth; ++j)
        {
            if (jmax >= j - kernel_size && jmax != 0)
            {
                for (k1 = i - kernel_size; k1 <= i + kernel_size; ++k1)
                {
                    if (k1 < 0 || k1 >= width)
                        continue;

                    k2 = j + kernel_size;

                    if (k2 < 0 || k2 >= heigth)
                        continue;

                    uchar pix = src.at<uchar>(k2, k1);
                    max = std::max(max, pix);
                    if (max == pix)
                    {
                        imax = k1;
                        jmax = k2;
                    }
                }
            }
            else
            {
                max = 0;
                for (k1 = i - kernel_size; k1 <= i + kernel_size; ++k1)
                {
                    if (k1 < 0 || k1 >= width)
                        continue;

                    for (k2 = j - kernel_size; k2 <= j + kernel_size; ++k2)
                    {
                        if (k2 < 0 || k2 >= heigth)
                            continue;

                        uchar pix = src.at<uchar>(k2, k1);
                        max = std::max(max, pix);
                        if (max == pix)
                        {
                            imax = k1;
                            jmax = k2;
                        }
                    }
                }
            }

            dst.at<uchar>(j, i) = max;
        }
    }

    std::cout << "------ Dilation done" << std::endl;

    return dst;
}

Mat erode_image(const Mat &src)
{
    std::cout << "------ Erosion..." << std::endl;
    int kernel_size = 6;
    int i, j, k1, k2;
    int heigth = src.rows;
    int width = src.cols;
    Mat dst(Size(width, heigth), src.type());

    uchar max = 255;
    int imax = 0;
    int jmax = 0;

# pragma omp parallel for collapse(3)
    for (i = 0; i < width; ++i)
    {
        imax = 0;
        jmax = 0;
        for (j = 0; j < heigth; ++j)
        {
            if (jmax >= j - kernel_size && jmax != 0)
            {
                for (k1 = i - kernel_size; k1 <= i + kernel_size; ++k1)
                {
                    if (k1 < 0 || k1 >= width)
                        continue;

                    k2 = j + kernel_size;

                    if (k2 < 0 || k2 >= heigth)
                        continue;

                    uchar pix = src.at<uchar>(k2, k1);
                    max = std::min(max, pix);
                    if (max == pix)
                    {
                        imax = k1;
                        jmax = k2;
                    }
                }
            }
            else
            {
                max = 255;
                for (k1 = i - kernel_size; k1 <= i + kernel_size; ++k1)
                {
                    if (k1 < 0 || k1 >= width)
                        continue;

                    for (k2 = j - kernel_size; k2 <= j + kernel_size; ++k2)
                    {
                        if (k2 < 0 || k2 >= heigth)
                            continue;

                        uchar pix = src.at<uchar>(k2, k1);
                        max = std::min(max, pix);
                        if (max == pix)
                        {
                            imax = k1;
                            jmax = k2;
                        }
                    }
                }
            }

            dst.at<uchar>(j, i) = max;
        }
    }

    std::cout << "------ Erosion done" << std::endl;

    return dst;
}


Mat top_hat_black(const Mat &src)
{
    std::cout << "-- Top hat Black..." << std::endl;
    int heigth = src.rows;
    int width = src.cols;

    Mat dst(Size(width, heigth), src.type());
    dst = dilate_image(src);

    Mat dst2(Size(width, heigth), src.type());
    dst2 = erode_image(dst);

    Mat result(Size(width, heigth), src.type());

    # pragma omp parallel for collapse(2)
    for (int i = 0; i < width; ++i)
        for (int j = 0; j < heigth; ++j)
            result.at<uchar>(j, i) = dst2.at<uchar>(j, i) - src.at<uchar>(j, i);


    std::cout << "-- Top Hat Black done" << std::endl << std::endl;
    return result;
}

Mat closing(const Mat &src)
{
    std::cout << "-- Closing..." << std::endl;
    int heigth = src.rows;
    int width = src.cols;

    Mat dst(Size(width, heigth), src.type());
    dst = erode_image(src);

    Mat dst2(Size(width, heigth), src.type());
    dst2 = dilate_image(dst2);

    std::cout << "-- Closing done" << std::endl;
    return dst2;
}

Mat top_hat_black_opencv(const Mat &src, Mat &dst)
{
    std::cout << "-- Top Hat Black OPENCV..." << std::endl;

    int kernel_size = 5;
    Mat element = getStructuringElement(0, Size(2 * kernel_size + 1,
                2 * kernel_size + 1), Point(kernel_size, kernel_size));
    morphologyEx(src, dst, MORPH_BLACKHAT, element);

    std::cout << "-- Top Hat Black OPENCV done" << std::endl;
    return dst;
}
