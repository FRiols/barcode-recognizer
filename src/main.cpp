#include "../include/main.hh"
#include "../include/threshold.hh"
#include "../include/topHat.hh"
#include "../include/finder.hh"
#include "../include/shapeRecognition.hh"
#include <string>

using namespace cv;
using namespace std::chrono;

void display(Mat image, std::string name)
{
    if (!g_debug)
        return;
    const char *name_window = name.c_str();
    cvNamedWindow(name_window, WINDOW_NORMAL);
    //startWindowThread();
    imshow(name_window, image);
}

void no_barcode(Mat &original)
{
    std::cout << "Pas de code barres" << std::endl;
    putText(original, "No barcode.", Point(100, 100),
            FONT_HERSHEY_TRIPLEX, 2, Scalar(255, 255, 255), 1, CV_AA);
    display(original, "No barcode.");
}

bool testFunction(Mat &original, const Mat &test)
{
    int heigth = original.rows;
    int width = original.cols;

    bool noBarCode = true;

    for (int i = 0; i < width; ++i)
    {
        if (noBarCode == false)
            break;

        for (int j = 0; j < heigth; ++j)
        {
            if (test.at<Vec3b>(j, i) != Vec3b(0, 0, 0))
                noBarCode = false;
        }
    }

    if (noBarCode)
    {
        std::cout << "Pas de code barres" << std::endl;
        putText(original, "No barcode.", Point(100, 100),
                FONT_HERSHEY_TRIPLEX, 2, Scalar(255, 255, 255), 1, CV_AA);
        if (g_debug)
            display(original, "No barcode.");

        return true;
    }

    return false;
}

Mat bin_and_tophat(const Mat &src)
{
    // ----- Top Hat Black
    Mat tophat_img = top_hat_black(src);
    tophat_img = reduce_num_colors(tophat_img);
    //display(tophat_img, "Top-hat black");

    // ----- Binarisation
    Mat bin_img;
    bin_img = threshold(src);
    //display(bin_img, "Binarisation");

    // ----- Top Hat + Binarisation
    Mat combo_img = tophat_with_binarisation(tophat_img, bin_img);
    //display(combo_img, "Tophat + binarisation");

    return combo_img;
}

Mat contours_first_steps(const Mat &src, ShapeRecognition *ShRec)
{
    Mat rect_img = ShRec->big_contours_recognition(src);
    //display(rect_img, "Contours recognition");

    Mat less_contours_img = ShRec->remove_useless_contours(rect_img);
    //display(less_contours_img, "Contours useless recognition");

    for (int i = 0; i < 4; ++i)
        less_contours_img = ShRec->remove_useless_contours(less_contours_img);
    //display(less_contours_img, "Removed useless contours");

    return less_contours_img;
}

Mat false_barcode(const Mat &src, ShapeRecognition *ShRec)
{
    Mat false_bc_img;
    std::vector<Cluster>::iterator it;
    for (it = ShRec->Clusters.begin(); it != ShRec->Clusters.end(); ++it)
    {
        if (it->getSize() == 0)
            continue;

        false_bc_img = it->remove_false_barcode(src);
        display(false_bc_img, "Faux code barres");
    }

    return false_bc_img;
}

int main(int argc, char *argv[])
{
    std::string true_debuging = "true";
    std::string false_debuging = "false";
    if (argc == 3)
    {
        if (false_debuging.compare(argv[2]) == 0)
            g_debug = false;
    }

    if (argc <= 1)
        std::cout << "Argument missing" << std::endl;

    IplImage *image = 0;
    image = cvLoadImage(argv[1], 1);
    Mat img(image);

    // On redimensionne l'image pour l'afficher plus simplement
    const int new_width = (int) ((float) image->width * 0.35);
    const int new_height = (int) ((float) image->height * 0.35);
    IplImage* new_img = cvCreateImage(cvSize(image->width, image->height), image->depth, image->nChannels);
    cvResize(image, new_img);

    Mat new_mat(new_img);

    if (img.empty())
    {
        std::cout << "No Data" << std::endl;
        return 1;
    }

    resize(img, img, Size(1024, 768), 0, 0, INTER_CUBIC);
    Mat grey_img;
    cvtColor(img, grey_img, CV_BGR2GRAY);
    cvtColor(new_mat, new_mat, CV_BGR2GRAY);
    display(img, "Original");

    // ----- Top Hat + Binarisation
    Mat combo_img = bin_and_tophat(grey_img);

    // ----- Creation of a new ShapeRecognition class
    ShapeRecognition *ShRec = new ShapeRecognition();

    // ----- Contours recognition + remove some useless contours
    Mat less_contours_img = contours_first_steps(combo_img, ShRec);

    // ----- Tests if there is a barcode
    bool noBarCode = testFunction(img, less_contours_img);
    if (noBarCode)
    {
        if (g_debug)
            waitKey(0);
        return 0;
    }

    Mat clusters_img = ShRec->make_clusters(less_contours_img);
    //display(clusters_img, "Clusters");


    std::vector<Cluster>::iterator it;
    Mat classified_img;
    for (it = ShRec->Clusters.begin(); it != ShRec->Clusters.end(); ++it)
        it->compute_angle();

    Mat false_bc_img = false_barcode(clusters_img, ShRec);
    noBarCode = testFunction(img, false_bc_img);
    if (noBarCode)
    {
        if (g_debug)
            waitKey(0);
        return 0;
    }

    ShRec->ctrs_ids.clear();
    for (int i = 0; i < ShRec->Clusters.size(); ++i)
    {
        std::pair<std::vector<Point>, int> tmpPair;
        for (int j = 0; j < ShRec->Clusters.at(i).getSize(); ++j)
        {
            tmpPair.first = ShRec->Clusters.at(i).contours_.at(j);
            tmpPair.second = ShRec->Clusters.at(i).ids_.at(j);
            ShRec->ctrs_ids.push_back(tmpPair);
        }
    }

    for (it = ShRec->Clusters.begin(); it != ShRec->Clusters.end(); ++it)
    {
        classified_img = it->remove_little_angle_occurences(false_bc_img);
        //display(classified_img, "Angles repetition");
    }

    ShRec->ctrs_ids.clear();
    for (int i = 0; i < ShRec->Clusters.size(); ++i)
    {
        std::pair<std::vector<Point>, int> tmpPair;
        for (int j = 0; j < ShRec->Clusters.at(i).getSize(); ++j)
        {
            tmpPair.first = ShRec->Clusters.at(i).contours_.at(j);
            tmpPair.second = ShRec->Clusters.at(i).ids_.at(j);
            ShRec->ctrs_ids.push_back(tmpPair);
        }
    }


    ShRec->Clusters.clear();
    ShRec->clusters.clear();
    Mat clusters_img2 = ShRec->make_clusters(classified_img);
    //display(clusters_img2, "Clusters2");

    Mat classified_img2;
    for (it = ShRec->Clusters.begin(); it != ShRec->Clusters.end(); ++it)
    {
        it->compute_angle();
        classified_img2 = it->remove_little_angle_occurences(false_bc_img);
        display(classified_img2, "Angles repetition2");
    }


/*    int numClusters = 0;
    Mat rectBar_img(classified_img);
    for (int i = 0; i < ShRec->Clusters.size(); ++i)
    {
        Cluster tmp = ShRec->Clusters.at(i);
        if (tmp.getSize() == 0)
            continue;
        rectBar_img = tmp.drawRectangle(img);
        display(rectBar_img, "Rectangle");
        ++numClusters;
    }
    std::cout << std::endl << numClusters << " clusters" << std::endl;
*/
    Mat disprop_img;
    for (it = ShRec->Clusters.begin(); it != ShRec->Clusters.end(); ++it)
    {
        disprop_img = it->disproportional_barcode(classified_img2);
        display(disprop_img, "Disproportional barcode");
    }

    ShRec->ctrs_ids.clear();
    for (int i = 0; i < ShRec->Clusters.size(); ++i)
    {
        std::pair<std::vector<Point>, int> tmpPair;
        for (int j = 0; j < ShRec->Clusters.at(i).getSize(); ++j)
        {
            tmpPair.first = ShRec->Clusters.at(i).contours_.at(j);
            tmpPair.second = ShRec->Clusters.at(i).ids_.at(j);
            ShRec->ctrs_ids.push_back(tmpPair);
        }
    }


    int numClusters = 0;
    Mat rectBar_img2(disprop_img);
    for (int i = 0; i < ShRec->Clusters.size(); ++i)
    {
        Cluster tmp = ShRec->Clusters.at(i);
        if (tmp.getSize() == 0)
            continue;
        rectBar_img2 = tmp.drawRectangle(img);
        display(rectBar_img2, "Rectangle2");
        ++numClusters;
    }
    std::cout << std::endl << numClusters << " clusters" << std::endl;
    if (numClusters == 0)
    {
        no_barcode(img);
        if (g_debug)
            waitKey(0);
        return 0;
    }



    noBarCode = testFunction(img, disprop_img);
    if (noBarCode)
    {
        if (g_debug)
            waitKey(0);
        return 0;
    }

    if (g_debug)
        waitKey(0);
    return numClusters;
}
